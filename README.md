TrackList
This project was generated with Angular CLI version 1.7.2

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

ng build --base-href "link here". This should be used when created dist folder is not the parent folder.

## Start server

Start server from server.js file using "node server.js" from server folder