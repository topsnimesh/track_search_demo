import { Component } from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {environment} from '../environments/environment'
import 'rxjs/add/operator/map'
import swal from 'sweetalert2'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Track List';
  dataLoaded = false;;
  trackList;
  searchText;
  cartList = [];
  favList = [];
  appBaseUrl : string = environment.appBaseUrl;


  constructor(private http : Http) {}

  ngOnInit(){
      this.getCouponList().subscribe(res => {
        this.trackList = res.data.tracks
        this.dataLoaded = true;
      })
    }

    getCouponList(){
       return this.http.get(this.appBaseUrl+'getlist')
      .map((res : Response) => res.json())
    }

    onSearch(){
      let params = {
        search : this.searchText 
      }
      return this.http.post(this.appBaseUrl+"search", params).subscribe(res => {
          var response = res.json();
          this.trackList = response.data
        });

    }

    add(id){
          swal({
              title: 'Thank You!',
              text: 'Your Selected Track is added to Cart',
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Okay',
          }).then((result) => {
            if (result.value) {
              for(var i=0; i<this.trackList.length; i++){
                if(this.trackList[i].id == id){
                  this.trackList[i].cartFlag = true;
                  this.cartList.push(this.trackList[i])
                }
              } 
              // this.ngOnInit();   
            }
          }) 
      } 

      remove(id){
        swal({
              title: 'Are You Sure!',
              text: 'Your Track will be removed from cart',
              type: 'warning',
              showCancelButton: false,
              confirmButtonText: 'Okay',
              confirmButtonClass :"btn-danger"
          }).then((result) => {
            if (result.value) {
              for(var i=0; i<this.cartList.length; i++){
                if(this.cartList[i].id == id){
                  this.trackList[i].cartFlag = false
                  this.cartList.pop()
                }
              } 
              // this.ngOnInit();   
            }
          })
      }
      addFav(id){
          swal({
              title: 'Thank You!',
              text: 'Your Selected Track is added to Favourites',
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Okay',
          }).then((result) => {
            if (result.value) {
              for(var i=0; i<this.trackList.length; i++){
                if(this.trackList[i].id == id){
                  this.trackList[i].favFlag = true
                  this.favList.push(this.trackList[i])
                }
              } 
              // this.ngOnInit();   
            }
          }) 
      }

      removeFav(id){
         swal({
              title: 'Are You Sure!',
              text: 'Your Track will be removed from favourites',
              type: 'warning',
              showCancelButton: false,
              confirmButtonText: 'Okay',
              confirmButtonClass :"btn-danger"
          }).then((result) => {
            if (result.value) {
              for(var i=0; i<this.favList.length; i++){
                if(this.favList[i].id == id){
                  this.trackList[i].favFlag = false
                  this.favList.pop()
                }
              } 
              // this.ngOnInit();   
            }
          }) 
      } 
}

