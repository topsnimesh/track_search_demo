import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DatePipe } from '@angular/common'

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
      ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
    
    // RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
