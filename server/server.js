const express = require('express')
const bodyParser = require('body-parser')
const app = express()
var cors = require('cors') 


app.use(bodyParser.json())
app.set('port', (process.env.PORT || 4000))
// var testService  = require('./service/test.service');

app.use(cors())
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(express.static('../dist'));
app.use(express.static('src'));

app.use('/api', require('./controller/list.controller'));


  app.listen(app.get('port'), function () {
    console.log('*Track List is listening on port:' + app.get('port'))
  })



    
    


