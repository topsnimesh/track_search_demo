var express = require('express');
var router = express.Router();
var request = require('request-promise'); 
 
var apiResponse = require('../apiResponse');

router.get('/getlist/', getList);
router.post('/search', getFilteredData);

async function getList(req, res) {
    try {
        const userList = await request('https://api.runerl.dk/api/search/tracks/?order=desc&from=0', { json: true }, (err, res, body) => {
            if (err) { 
                    return console.log(err); 
            }
            });
        const apiSuccessResponse = apiResponse.setSuccessResponse(200, "User list found.", userList);
        res.json(apiSuccessResponse).end();
    }
    catch(err) {
        var apiFailureResponse = apiResponse.setFailureResponse(err);
        res.json(apiFailureResponse).end();
    };

}

async function getFilteredData(req, res){
    var filteredData =[];
    try {
        const userList = await request('https://api.runerl.dk/api/search/tracks/?order=desc&from=0', { json: true }, (err, res, body) => {
            if (err) { 
                    return console.log(err); 
            }
            });
        if(req.body != {} && req.body.search != ""){
            var search = req.body.search.toLowerCase();
            for(var i=0; i<userList.tracks.length; i++){
                for(var j=0; j<userList.tracks[i].genres.length; j++){
                    var genres = userList.tracks[i].genres[j].title.toLowerCase();
                    if(genres == search){
                        filteredData.push(userList.tracks[i])
                    }
                }
                for(var j=0; j<userList.tracks[i].moods.length; j++){
                    var moods = userList.tracks[i].moods[j].title.toLowerCase();
                    if(moods == search){
                        filteredData.push(userList.tracks[i])
                    }
                }
            }
        }
        else{
            for(var i=0; i<userList.tracks.length; i++){
                filteredData.push(userList.tracks[i])
            }
        }
            const apiSuccessResponse = apiResponse.setSuccessResponse(200, "User list found.", filteredData);
            res.json(apiSuccessResponse).end();
        
    }
    catch(err) {
        var apiFailureResponse = apiResponse.setFailureResponse(err);
        res.json(apiFailureResponse).end();
    };  
}


 
module.exports = router;