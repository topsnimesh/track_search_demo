var setSuccessResponse = function (res_code,message,data)
{
	var apiResponse = {
		"code":res_code,
		"message":message,
		"data":data
	}
	return apiResponse;
}

var setFailureResponse = function(err){
	var apiResponse = {
		"code":400,
		"message":"Bad Request or Internal Server Error. Please see data for inner exception.",
		"data":{"errMsg":err}
	}
	return apiResponse;
}

module.exports.setSuccessResponse = setSuccessResponse;
module.exports.setFailureResponse = setFailureResponse;